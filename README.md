# Year in Review - Wikimedia Stats Celebration Project

## Background

The project was originally created by [Jon Robson](https://jdlrobson.com/). The relevant links to the original project are as follows:
- [GitHub](https://github.com/jdlrobson/wikipedia-year-in-review )
- [Deployed application](https://wikipediayir.netlify.app/)

While the original project will continue to receive updates, *this project* is an attempt to keep the tool within the Wikimedia ecosystem.

Some noteable differences between the original project and this version are as follows:
- The original application is deployed on Netlify while this tool is on Toolforge 
- The original application uses Google Analytics to analyse site traffic while this tool does not
- This tool uses the Codex component library for its UI

## Description

Year in Review is a project aimed at celebrating the statistics and achievements of Wikimedia contributors over the past year. This Vue.js project provides an interactive interface for users to explore and visualize their contributions and milestones.

## Installation

### Clone the Repository

```bash
git clone https://gitlab.wikimedia.org/toolforge-repos/yearinreview
```

### Run the project

- Go to project directory `cd year-in-review`
- Install Dependencies `npm ci`
- Run the Project `npm start`

### Access the Application
Open your web browser and navigate to http://localhost:1234 to access the web app!

### Usage

Upon accessing the application, users can view a summary of their contributions and statistics for any previous year.

### Contributions
Contributions to the Year in Review project are welcome! If you'd like to contribute, please follow these steps:

- Fork the repository on Gitlab and clone it on your system.
- Create a new branch for your feature or bug fix.
- Test your changes thoroughly.
- Commit your changes and create a pull request with a descriptive title and explanation of your changes.
